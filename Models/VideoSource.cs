﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Models
{
    public class VideoSource
    {
        public string IdVideoSource { get; set; }
        public string NameVideoSource { get; set; }
        public Broadcast Broadcast { get; set; }
        public string IdInternetSource { get; set; }
        public string NameInternetSource { get; set; }

        public Video Video { get; set; }

    }
}
