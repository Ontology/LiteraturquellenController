﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Models
{
    public class Video
    {
        public string IdVideo { get; set; }
        public string NameVideo { get; set; }
    }
}
