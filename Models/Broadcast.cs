﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Models
{
    public class Broadcast
    {
        public string IdVideoSource { get; set; }
        public string IdBroadcast { get; set; }
        public string NameBroadcast { get; set; }
        public string IdAttributeDateTimeStamp { get; set; }
        public DateTime? BroadcastDate { get; set; }
        public string IdAuthor { get; set; }
        public string NameAuthor { get; set; }
        public string IdShow { get; set; }
        public string NameShow { get; set; }
        public string IdBroadcastStation { get; set; }
        public string NameBroadcastStation { get; set; }
    }
}
