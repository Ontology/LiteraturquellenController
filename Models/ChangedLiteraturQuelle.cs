﻿using LiteraturquellenController.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Models
{
    public class ChangedLiteraturQuelle
    {
        public string Uid { get; set; }
        public string PropertyName { get; set; }
        public LiteraturQuelle LiteraturQuelle { get; set; }
    }
}
