﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Models
{
    [KendoGridConfig(width = "100%", groupbable = true, sortable = true, autoBind = true, selectable = "cell", editable = "inline")]
    [KendoPageable(refresh = true, pageSizes = new int[] { 5, 10, 15, 20, 50, 100, 1000 }, buttonCount = 5, pageSize = 20)]
    [KendoFilter(extra = true)]
    [KendoStringFilterable(contains = "Contains", startsWith = "Starts with", eq = "Is equal to", neq = "Is not equal to", isnotnull = "Not null", isnull = "Null", isempty = "Empty")]
    public class LiteraturQuelle
    {
        [KendoColumn(hidden = true)]
        public string IdLiteraturQuelle { get; set; }

        [KendoColumn(hidden = false, Order = 1, title = "Literatur-Quelle", width = "200px", filterable = true)]
        public string NameLiteraturQuelle { get; set; }

        [KendoColumn(hidden = true)]
        public string IdQuelle { get; set; }

        [KendoColumn(hidden = false, Order = 2, title = "Quelle", width = "200px", filterable = true)]
        public string NameQuelle { get; set; }

        [KendoColumn(hidden = true)]
        public string IdClassQuelle { get; set; }

        [KendoColumn(hidden = false, Order = 2, title = "Quellen-Klasse", width = "200px", filterable = true)]
        public string NameClassQuelle { get; set; }
    }
}
