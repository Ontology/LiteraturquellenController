﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Models
{
    public class SelectedLiteraturQuelle
    {
        public string Uid { get; set; }
        public string Property { get; set; }
        public LiteraturQuelle LiteraturQuelle { get; set; }
    }
}
