﻿using LiteraturquellenController.Models;
using LiteraturquellenController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LiteraturquellenController.Services
{
    public class ElasticServiceAgent : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReaderInternetSource;

        private clsTransaction transaction;
        private clsRelationConfig relationConfig;

        private object serviceLocker = new object();
        private clsOntologyItem refItem;

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        private VideoSourceResult videoSourceResult;
        public VideoSourceResult VideoSourceResult
        {
            get { return videoSourceResult; }
            set
            {
                videoSourceResult = value;
                RaisePropertyChanged(nameof(VideoSourceResult));
            }
        }

        private ResultCreateLiteraturQuelle resultCreateQuelle;
        public ResultCreateLiteraturQuelle ResultCreateQuelle
        {
            get
            {
                lock(serviceLocker)
                {
                    return resultCreateQuelle;
                }
                
            }
            set
            {
                lock(serviceLocker)
                {
                    resultCreateQuelle = value;
                }

                RaisePropertyChanged(nameof(ResultCreateQuelle));
            }
        }

        private LiteraturQuellenResult resultLiteraturSource;
        public LiteraturQuellenResult ResultLiteraturSource
        {
            get
            {
                lock(serviceLocker)
                {
                    return resultLiteraturSource;
                }   
            }
            set
            {
                lock(serviceLocker)
                {
                    resultLiteraturSource = value;
                }

                RaisePropertyChanged(nameof(ResultLiteraturSource));
            }
        }

        public List<clsObjectRel> UrlRels { get; set; }
        public List<clsObjectRel> CreatorRels { get; set; }
        public List<clsObjectRel> LogEntryRels { get; set; }

        private clsOntologyItem resultSaveUrl;
        public clsOntologyItem ResultSaveUrl
        {
            get { return resultSaveUrl; }
            set
            {
                resultSaveUrl = value;
                RaisePropertyChanged(nameof(ResultSaveUrl));
            }
        }

        private clsOntologyItem resultSaveAuthor;
        public clsOntologyItem ResultSaveAuthor
        {
            get { return resultSaveAuthor; }
            set
            {
                resultSaveAuthor = value;
                RaisePropertyChanged(nameof(ResultSaveAuthor));
            }
        }

        public void StopRead()
        {
            if (getSourceDataAsync != null)
            {
                try
                {
                    getSourceDataAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }
        }

        private Thread getSourceDataAsync;

        public void GetSourceDataByInternetSource(clsOntologyItem refItem)
        {
            this.refItem = refItem;
            if (getSourceDataAsync != null)
            {
                try
                {
                    getSourceDataAsync.Abort();
                }
                catch(Exception ex)
                {

                }
            }

            getSourceDataAsync = new Thread(GetSourceDataByInternetSourceAsync);
            getSourceDataAsync.Start();
        }

        public async Task<LiteraturQuellenResult> GetLiteraturSources(clsOntologyItem refItem = null)
        {
            var resultItem = new LiteraturQuellenResult
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            resultItem.LiteraturSources = new List<clsOntologyItem>();

            if (refItem == null)
            {
                var search = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = localConfig.OItem_type_literarische_quelle.GUID
                    }
                };

                var dbReader = new OntologyModDBConnector(localConfig.Globals);

                resultItem.Result = dbReader.GetDataObjects(search);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID) return resultItem;

                resultItem.LiteraturSources = dbReader.Objects1;
            }
            else if (refItem.GUID_Parent == localConfig.OItem_type_literarische_quelle.GUID)
            {
                resultItem.LiteraturSources.Add(refItem);
            }
            else
            {
                var searchResult = Get001_RefToLiteraturSourceLeftRight(refItem);

                resultItem.Result = searchResult.Result;

                if (searchResult.Result.GUID == localConfig.Globals.LState_Error.GUID) return resultItem;


                resultItem.RefToLiteraturSourceLeftRight = searchResult.ResultList;

                var resultRefRightLeft = Get001_RefToLiteraturSourceRightLeft(refItem);

                resultItem.Result = resultRefRightLeft.Result;

                if (resultRefRightLeft.Result.GUID == localConfig.Globals.LState_Error.GUID) return resultItem;

                resultItem.RefToLiteraturSourceRightLeft = resultRefRightLeft.ResultList;

                resultItem.LiteraturSources = resultItem.RefToLiteraturSourceRightLeft.Select(refTo => new clsOntologyItem
                {
                    GUID = refTo.ID_Other,
                    Name = refTo.Name_Other,
                    GUID_Parent = refTo.ID_Parent_Other,
                    Type = localConfig.Globals.Type_Object
                }).ToList();

                resultItem.LiteraturSources.AddRange(resultItem.RefToLiteraturSourceRightLeft.Select(refTo => new clsOntologyItem
                {
                    GUID = refTo.ID_Object,
                    Name = refTo.Name_Object,
                    GUID_Parent = refTo.ID_Parent_Object,
                    Type = localConfig.Globals.Type_Object
                }));

            }

            var sourceResult = Get003_SourceToLiteraturSource(resultItem.LiteraturSources, localConfig.OItem_relationtype_issubordinated.GUID);

            resultItem.Result = sourceResult.Result;

            if (sourceResult.Result.GUID == localConfig.Globals.LState_Error.GUID) return resultItem;

            resultItem.SourceToLiteraturSource = sourceResult.ResultList;

            ResultLiteraturSource = resultItem;

            return resultItem;
        }


        public async Task<InternetSourceResult> GetInternetSourcesByRef(clsOntologyItem refItem)
        {
            var resultRefLeftRight = Get001_RefToLiteraturSourceLeftRight(refItem);

            if (resultRefLeftRight.Result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            var resultRefRightLeft = Get001_RefToLiteraturSourceRightLeft(refItem);

            if (resultRefRightLeft.Result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            var resultInternetSourceToLitSource = Get002_InternetSourceToLitSource(resultRefLeftRight.ResultList, resultRefRightLeft.ResultList);

            if (resultInternetSourceToLitSource.Result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            var resultInternetSourceToUrls = Get003_InternetSourceTo(resultInternetSourceToLitSource.ResultList, localConfig.OItem_relationtype_belonging_source.GUID, localConfig.OItem_type_url.GUID);

            if (resultInternetSourceToUrls.Result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            var resultInternetSourceToPartner = Get003_InternetSourceTo(resultInternetSourceToLitSource.ResultList, localConfig.OItem_relationtype_ersteller.GUID, localConfig.OItem_type_partner.GUID);

            if (resultInternetSourceToPartner.Result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            var result = new InternetSourceResult
            {
                RefToLiteraturSourceLeftRight = resultRefLeftRight.ResultList,
                RefToLeteraturSourceRightLeft = resultRefRightLeft.ResultList,
                InternetSourceToLiteraturSource = resultInternetSourceToLitSource.ResultList,
                CreatorRels = resultInternetSourceToPartner.ResultList,
                UrlRels = resultInternetSourceToUrls.ResultList
            };

            return result;
        }

        public async Task<ResultCreateLiteraturQuelle> CreateLiteraturQuelle(clsOntologyItem quellType, string nameQuelle, clsOntologyItem oItemRef)
        {
            var resultItem = new ResultCreateLiteraturQuelle
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };


            if (quellType == null || string.IsNullOrEmpty(nameQuelle))
            {
                resultItem.Result = localConfig.Globals.LState_Error.Clone();
                ResultCreateQuelle = resultItem;
                return resultItem;
            }

            transaction.ClearItems();

            clsOntologyItem source = null;
            var oItemLiteraturQuelle = new clsOntologyItem
            {
                GUID = localConfig.Globals.NewGUID,
                Name = nameQuelle,
                GUID_Parent = localConfig.OItem_type_literarische_quelle.GUID,
                Type = localConfig.Globals.Type_Object
            };

            resultItem.Result = transaction.do_Transaction(oItemLiteraturQuelle);

            if (resultItem.Result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                source = new clsOntologyItem
                {
                    GUID = localConfig.Globals.NewGUID,
                    Name = nameQuelle,
                    GUID_Parent = quellType.GUID,
                    Type = localConfig.Globals.Type_Object
                };

                resultItem.Result = transaction.do_Transaction(source);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transaction.rollback();
                }

                if (resultItem.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var rel = relationConfig.Rel_ObjectRelation(source, oItemLiteraturQuelle, localConfig.OItem_relationtype_issubordinated);
                    resultItem.Result = transaction.do_Transaction(rel);
                }

                if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transaction.rollback();
                }
            }

            
            if (resultItem.Result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (oItemRef != null)
                {
                    var rel = relationConfig.Rel_ObjectRelation(oItemLiteraturQuelle, oItemRef, localConfig.OItem_relationtype_belongsto);
                    resultItem.Result = transaction.do_Transaction(rel);

                    if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                    }
                }
            }

            resultItem.LiteraturQuelle = new LiteraturQuelle
            {
                IdLiteraturQuelle = oItemLiteraturQuelle.GUID,
                NameLiteraturQuelle = oItemLiteraturQuelle.Name,
                IdQuelle = source.GUID,
                NameQuelle = source.Name,
                IdClassQuelle = source.GUID_Parent,
                NameClassQuelle = source.Name_Parent
            };
            ResultCreateQuelle = resultItem;
            
            
            return resultItem;
        }

        private SearchResult Get001_RefToLiteraturSourceLeftRight(clsOntologyItem refItem)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchRef = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = refItem.GUID,
                    ID_Parent_Object = refItem.GUID_Parent,
                    ID_Parent_Other = localConfig.OItem_type_literarische_quelle.GUID
                }
            };

            var result = dbReader.GetDataObjectRel(searchRef);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                return new SearchResult
                {
                    ResultList = dbReader.ObjectRels,
                    Result = result
                };
            }
            else
            {
                return new SearchResult
                {
                    ResultList = null,
                    Result = result
                };
            }
        }

        private SearchResult Get001_RefToLiteraturSourceRightLeft(clsOntologyItem refItem)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchRef = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = refItem.GUID,
                    ID_Parent_Other = refItem.GUID_Parent,
                    ID_Parent_Object = localConfig.OItem_type_literarische_quelle.GUID
                }
            };

            var result = dbReader.GetDataObjectRel(searchRef);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                return new SearchResult
                {
                    ResultList = dbReader.ObjectRels,
                    Result = result
                };
            }
            else
            {
                return new SearchResult
                {
                    ResultList = null,
                    Result = result
                };
            }
        }

        private SearchResult Get002_InternetSourceToLitSource(List<clsObjectRel> refToLitSourceLeftRight, 
            List<clsObjectRel> refToLitSourceRightLeft)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchUrls = refToLitSourceLeftRight.Select(refToLit => new clsObjectRel
            {
                ID_Other = refToLit.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_issubordinated.GUID,
                ID_Parent_Object = localConfig.OItem_type_internet_quellenangabe.GUID
            }).ToList();

            searchUrls.AddRange(refToLitSourceRightLeft.Select(refToLit => new clsObjectRel
            {
                ID_Other = refToLit.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_issubordinated.GUID,
                ID_Parent_Object = localConfig.OItem_type_internet_quellenangabe.GUID
            }));

            if (searchUrls.Any())
            {
                var result = dbReader.GetDataObjectRel(searchUrls);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    return new SearchResult
                    {
                        Result = result,
                        ResultList = dbReader.ObjectRels
                    };
                }
                else
                {
                    return new SearchResult
                    {
                        Result = result,
                        ResultList = null
                    };
                }
            }
            else
            {
                return new SearchResult
                {
                    Result = localConfig.Globals.LState_Success.Clone(),
                    ResultList = new List<clsObjectRel>()
                };
            }
        }

        private SearchResult Get003_SourceToLiteraturSource(List<clsOntologyItem> literaturSources, string idRelationType, string idParentOther = null)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchSourceTo = literaturSources.Select(litSource => new clsObjectRel
            {
                ID_Other = litSource.GUID,
                ID_RelationType = idRelationType,
                ID_Parent_Object = idParentOther
            }).ToList();

            if (searchSourceTo.Any())
            {
                var result = dbReader.GetDataObjectRel(searchSourceTo);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    return new SearchResult
                    {
                        Result = result,
                        ResultList = dbReader.ObjectRels
                    };
                }
                else
                {
                    return new SearchResult
                    {
                        Result = result,
                        ResultList = null
                    };
                }
            }
            else
            {
                return new SearchResult
                {
                    Result = localConfig.Globals.LState_Success.Clone(),
                    ResultList = new List<clsObjectRel>()
                };
            }
        }

        private SearchResult Get003_InternetSourceTo(List<clsObjectRel> internetSourceToLitSource, string idRelationType, string idParentOther)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchInternetSourceTo = internetSourceToLitSource.Select(inetSrcToLitSrc => new clsObjectRel
            {
                ID_Object = inetSrcToLitSrc.ID_Object,
                ID_RelationType = idRelationType,
                ID_Parent_Other = idParentOther
            }).ToList();

            if (searchInternetSourceTo.Any())
            {
                var result = dbReader.GetDataObjectRel(searchInternetSourceTo);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    return new SearchResult
                    {
                        Result = result,
                        ResultList = dbReader.ObjectRels
                    };
                }
                else
                {
                    return new SearchResult
                    {
                        Result = result,
                        ResultList = null
                    };
                }
            }
            else
            {
                return new SearchResult
                {
                    Result = localConfig.Globals.LState_Success.Clone(),
                    ResultList = new List<clsObjectRel>()
                };
            }
        }

        private void GetSourceDataByInternetSourceAsync()
        {
            var objORL_InternetQuelle = new List<clsObjectRel> {
                new clsObjectRel {ID_Object = refItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Other = localConfig.OItem_type_url.GUID },
                new clsObjectRel {ID_Object = refItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_ersteller.GUID,
                    ID_Parent_Other = localConfig.OItem_type_partner.GUID }
            };

            
            var result = dbReaderInternetSource.GetDataObjectRel(objORL_InternetQuelle, doIds: false);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                UrlRels = dbReaderInternetSource.ObjectRels.Where(isource => isource.ID_Parent_Other == localConfig.OItem_type_url.GUID).ToList();
                CreatorRels = dbReaderInternetSource.ObjectRels.Where(isource => isource.ID_Parent_Other == localConfig.OItem_type_partner.GUID).ToList();
            }
            else
            {
                UrlRels = new List<clsObjectRel>();
                CreatorRels = new List<clsObjectRel>();
            }
            
            ResultData = result;
        }

        public async Task<VideoSourceResult> GetSourceDataByVideoSource(clsOntologyItem oItemVideoSource)
        {
            var resultItem = new VideoSourceResult
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            lock(serviceLocker)
            {
                resultItem.VideoSources = new List<clsOntologyItem>
                {
                    oItemVideoSource
                };
                var searchBroadcasts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = oItemVideoSource.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_broadcasted_by.GUID,
                        ID_Parent_Other = localConfig.OItem_type_ausstrahlung.GUID
                    }
                };

                var dbReaderBroadcasts = new OntologyModDBConnector(localConfig.Globals);
                resultItem.Result = dbReaderBroadcasts.GetDataObjectRel(searchBroadcasts);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    VideoSourceResult = resultItem;
                    return resultItem;
                }

                resultItem.VideoSourceToBroadcast = dbReaderBroadcasts.ObjectRels;

                var searchVideo = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = oItemVideoSource.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_belonging.GUID,
                        ID_Parent_Other = localConfig.OItem_type_video.GUID
                    }
                };

                var dbReaderVideos = new OntologyModDBConnector(localConfig.Globals);
                resultItem.Result = dbReaderVideos.GetDataObjectRel(searchVideo);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    VideoSourceResult = resultItem;
                    return resultItem;
                }

                resultItem.VideoSourceToVideo = dbReaderVideos.ObjectRels;

                var searchBroadCastAttributes = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_AttributeType = localConfig.OItem_attribute_datetimestamp.GUID,
                        ID_Object = oItemVideoSource.GUID
                    }
                };

                var dbReaderBroadcastAttributes = new OntologyModDBConnector(localConfig.Globals);
                resultItem.Result = dbReaderBroadcastAttributes.GetDataObjectAtt(searchBroadCastAttributes);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    VideoSourceResult = resultItem;
                    return resultItem;
                }

                resultItem.BroadcastAttributes = dbReaderBroadcastAttributes.ObjAtts;

                var searchBroadcastRels = dbReaderBroadcasts.ObjectRels.Select(broadCast => new clsObjectRel
                {
                    ID_Object = broadCast.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_autor.GUID,
                    ID_Parent_Other = localConfig.OItem_type_partner.GUID
                }).ToList();

                searchBroadcastRels.AddRange(dbReaderBroadcasts.ObjectRels.Select(broadCast => new clsObjectRel
                {
                    ID_Object = broadCast.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_broadcasted_in.GUID,
                    ID_Parent_Other = localConfig.OItem_class_sendung.GUID
                }));

                searchBroadcastRels.AddRange(dbReaderBroadcasts.ObjectRels.Select(broadCast => new clsObjectRel
                {
                    ID_Object = broadCast.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_broadcasted_by.GUID,
                    ID_Parent_Other = localConfig.OItem_class_video_sender.GUID
                }));

                var dbReaderBroadcast = new OntologyModDBConnector(localConfig.Globals);
                if (searchBroadcastRels.Any())
                {
                    resultItem.Result = dbReaderBroadcast.GetDataObjectRel(searchBroadcastRels);

                    if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        VideoSourceResult = resultItem;
                        return resultItem;
                    }
                }
                

                resultItem.BroadcastToAuthor = dbReaderBroadcast.ObjectRels.Where(objRel => objRel.ID_Parent_Other == localConfig.OItem_type_partner.GUID).ToList();
                resultItem.BroadcastToStation = dbReaderBroadcast.ObjectRels.Where(objRel => objRel.ID_Parent_Other == localConfig.OItem_class_video_sender.GUID).ToList();
                resultItem.BroadcastToShow = dbReaderBroadcast.ObjectRels.Where(objRel => objRel.ID_Parent_Other == localConfig.OItem_class_sendung.GUID).ToList();

                var searchVideoSourceToInternetSource = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = oItemVideoSource.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_from.GUID,
                        ID_Parent_Other = localConfig.OItem_type_internet_quellenangabe.GUID
                    }
                };

                var dbReaderInternetSource = new OntologyModDBConnector(localConfig.Globals);
                resultItem.Result = dbReaderInternetSource.GetDataObjectRel(searchVideoSourceToInternetSource);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    resultItem.VideoSourceToInternetSource = dbReaderInternetSource.ObjectRels;
                }
            }


            VideoSourceResult = resultItem;
            return resultItem;
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            return dbReader.GetOItem(id, type);
        }


        public async Task<clsOntologyItem> SaveUrlOfInternetSource(clsOntologyItem oItemUrl, clsOntologyItem oItemInternetSource)
        {
            var rel = relationConfig.Rel_ObjectRelation(oItemInternetSource, oItemUrl, localConfig.OItem_relationtype_belonging_source);
            transaction.ClearItems();

            var result = transaction.do_Transaction(rel, true);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                rel.Name_Other = oItemUrl.Name;
                UrlRels = new List<clsObjectRel> { rel };
            }

            ResultSaveUrl = result;
            return result;
        }

        public async Task<clsOntologyItem> SaveAuthorOfInternetSource(clsOntologyItem oItemAuthor, clsOntologyItem oItemInternetSource)
        {
            var rel = relationConfig.Rel_ObjectRelation(oItemInternetSource, oItemAuthor, localConfig.OItem_relationtype_ersteller);
            transaction.ClearItems();

            var result = transaction.do_Transaction(rel, true);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                rel.Name_Other = oItemAuthor.Name;
                CreatorRels = new List<clsObjectRel> { rel };
            }

            ResultSaveAuthor = result;
            return result;
        }

        public ElasticServiceAgent(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        public ElasticServiceAgent(Globals globals)
        {
            localConfig = new clsLocalConfig(globals);
            Initialize();
        }

        private void Initialize()
        {
            dbReaderInternetSource = new OntologyModDBConnector(localConfig.Globals);
            transaction = new clsTransaction(localConfig.Globals);
            relationConfig = new clsRelationConfig(localConfig.Globals);
        }

    }

    public class SearchResult
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectRel> ResultList { get; set; }
    }

    public class InternetSourceResult
    {
        public List<clsObjectRel> RefToLiteraturSourceLeftRight { get; set; }
        public List<clsObjectRel> RefToLeteraturSourceRightLeft { get; set; }
        public List<clsObjectRel> InternetSourceToLiteraturSource { get; set; }
        public List<clsObjectRel> UrlRels { get; set; }
        public List<clsObjectRel> CreatorRels { get; set; }
    }

    public class VideoSourceResult
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> VideoSources { get; set; }
        public List<clsObjectRel> RefToLiteraturSourceLeftRight { get; set; }
        public List<clsObjectRel> RefToLeteraturSourceRightLeft { get; set; }
        public List<clsObjectRel> InternetSourceToLiteraturSource { get; set; }
        public List<clsObjectRel> VideoSourceToBroadcast { get; set; }
        public List<clsObjectRel> VideoSourceToInternetSource { get; set; }
        public List<clsObjectRel> VideoSourceToMediaItemRange { get; set; }
        public List<clsObjectRel> VideoSourceToVideo { get; set; }

        public List<clsObjectAtt> BroadcastAttributes { get; set; }
        public List<clsObjectRel> BroadcastToAuthor { get; set; }
        public List<clsObjectRel> BroadcastToStation { get; set; }
        public List<clsObjectRel> BroadcastToShow { get; set; }
    }

    public class LiteraturQuellenResult
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> LiteraturSources { get; set; }
        public List<clsObjectRel> RefToLiteraturSourceLeftRight { get; set; }
        public List<clsObjectRel> RefToLiteraturSourceRightLeft { get; set; }
        public List<clsObjectRel> SourceToLiteraturSource { get; set; }
    }

    public class ResultCreateLiteraturQuelle
    {
        public clsOntologyItem Result { get; set; }
        public LiteraturQuelle LiteraturQuelle { get; set; }
    }
}
