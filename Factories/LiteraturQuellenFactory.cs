﻿using ImportExport_Module.Base;
using LiteraturquellenController.Models;
using LiteraturquellenController.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Factories
{
    public class LiteraturQuellenFactory : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private object factoryLocker = new object();

        private ResultLiteraturQuellen resultLiteraturQuellen;
        public ResultLiteraturQuellen ResultLiteraturQuellen
        {
            get
            {
                lock(factoryLocker)
                {
                    return resultLiteraturQuellen;
                }
                
            }
            set
            {
                lock(factoryLocker)
                {
                    resultLiteraturQuellen = value;
                }

                RaisePropertyChanged(nameof(ResultLiteraturQuellen));
            }
        }

        public async Task<ResultLiteraturQuellen> CreateLiteraturQuellenList(LiteraturQuellenResult quellenResult)
        {
            var resultItem = new ResultLiteraturQuellen
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var literaturQuellen = new List<LiteraturQuelle>();
            lock(factoryLocker)
            {
                resultItem.LiteraturQuellen = quellenResult.SourceToLiteraturSource.Select(sourceTo => new LiteraturQuelle
                {
                    IdLiteraturQuelle = sourceTo.ID_Other,
                    NameLiteraturQuelle = sourceTo.Name_Other,
                    IdQuelle = sourceTo.ID_Object,
                    NameQuelle = sourceTo.Name_Object,
                    IdClassQuelle = sourceTo.ID_Parent_Object,
                    NameClassQuelle = sourceTo.Name_Parent_Object
                }).ToList();

            }

            ResultLiteraturQuellen = resultItem;
            return resultItem;
        }

        public LiteraturQuellenFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }

    public class ResultLiteraturQuellen
    {
        public clsOntologyItem Result { get; set; }
        public List<LiteraturQuelle> LiteraturQuellen { get; set; }

    }
}
