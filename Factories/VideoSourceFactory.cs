﻿using LiteraturquellenController.Models;
using LiteraturquellenController.Services;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Factories
{
    public class VideoSourceFactory : NotifyPropertyChange
    {
        private object factoryLocker = new object();

        private clsLogStates logStates = new clsLogStates();

        private ResultVideoSource resultVideoSource;
        public ResultVideoSource ResultVideoSource
        {
            get
            {
                return resultVideoSource;
            }
            set
            {
                resultVideoSource = value;
                RaisePropertyChanged(nameof(ResultVideoSource));
            }
        }

        public async Task<ResultVideoSource> CreateVideoSourceListAndWriteToJson(VideoSourceResult videoSourceResult)
        {
            var resultItem = new ResultVideoSource
            {
                Result = logStates.LogState_Success.Clone()
            };

            var broadCasts = (from broadCast in videoSourceResult.VideoSourceToBroadcast
                              join broadCastAttribute in videoSourceResult.BroadcastAttributes on broadCast.ID_Other equals broadCastAttribute.ID_Object into broadCastAttributes
                              from broadCastAttribute in broadCastAttributes.DefaultIfEmpty()
                              join author in videoSourceResult.BroadcastToAuthor on broadCast.ID_Other equals author.ID_Object into authors
                              from author in authors.DefaultIfEmpty()
                              join station in videoSourceResult.BroadcastToStation on broadCast.ID_Other equals station.ID_Object into stations
                              from station in stations.DefaultIfEmpty()
                              join show in videoSourceResult.BroadcastToShow on broadCast.ID_Other equals show.ID_Object into shows
                              from show in shows.DefaultIfEmpty()
                              select new Broadcast
                              {
                                  IdVideoSource = broadCast.ID_Object,
                                  IdBroadcast = broadCast.ID_Other,
                                  NameBroadcast = broadCast.Name_Other,
                                  IdAttributeDateTimeStamp = broadCastAttribute != null ? broadCastAttribute.ID_Attribute : null,
                                  BroadcastDate = broadCastAttribute != null ? broadCastAttribute.Val_Date : null,
                                  IdAuthor = author != null ? author.ID_Other : null,
                                  NameAuthor = author != null ? author.Name_Other : null,
                                  IdBroadcastStation = station != null ? station.ID_Other : null,
                                  NameBroadcastStation = station != null ? station.Name_Other : null,
                                  IdShow = show != null ? show.ID_Other : null,
                                  NameShow = show != null ? show.Name_Other : null
                              }).ToList();

            var videoSources = (from videoSource in videoSourceResult.VideoSources
                                join videoSourceToVideo in videoSourceResult.VideoSourceToVideo on videoSource.GUID equals videoSourceToVideo.ID_Object into videoSourcesToVideos
                                from videoSourceToVideo in videoSourcesToVideos.DefaultIfEmpty()
                                join videoSourceToInternetSource in videoSourceResult.VideoSourceToInternetSource on videoSource.GUID equals videoSourceToInternetSource.ID_Object into videoSourceToInternetSources
                                from videoSourceToInternetSource in videoSourceToInternetSources.DefaultIfEmpty()
                                select new VideoSource
                                {
                                    IdVideoSource = videoSource.GUID,
                                    NameVideoSource = videoSource.Name,
                                    IdInternetSource = videoSourceToInternetSource != null ? videoSourceToInternetSource.ID_Other : null,
                                    NameInternetSource = videoSourceToInternetSource != null ? videoSourceToInternetSource.Name_Other : null,
                                    Video = new Video
                                    {
                                        IdVideo = videoSourceToVideo != null ? videoSourceToVideo.ID_Other : null,
                                        NameVideo = videoSourceToVideo != null ? videoSourceToVideo.Name_Other : null
                                    }
                                }).ToList();

            videoSources.ForEach(videoSource =>
            {
                videoSource.Broadcast = broadCasts.FirstOrDefault(broadCast => broadCast.IdVideoSource == videoSource.IdVideoSource);
            });

            resultItem.VideoSource = videoSources.FirstOrDefault();

           
            ResultVideoSource = resultItem;
            return ResultVideoSource;
        }

      
    }

    public class ResultVideoSource
    {
        public clsOntologyItem Result { get; set; }
        public VideoSource VideoSource { get; set; }
    }
}
