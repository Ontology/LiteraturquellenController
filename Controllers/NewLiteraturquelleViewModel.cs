﻿using LiteraturquellenController.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Controllers
{
    public class NewLiteraturquelleViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(nameof(IsSuccessful_Login));

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(nameof(IsToggled_Listen));

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(nameof(Text_View));

            }
        }

        private KendoDropDownConfig kendodropdownconfig_SourceTypes;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.KendoDropDownList, ViewItemId = "dropDownSourceTypes", ViewItemType = ViewItemType.Content)]
		public KendoDropDownConfig KendoDropDownConfig_SourceTypes
        {
            get { return kendodropdownconfig_SourceTypes; }
            set
            {
                if (kendodropdownconfig_SourceTypes == value) return;

                kendodropdownconfig_SourceTypes = value;

                RaisePropertyChanged(nameof(KendoDropDownConfig_SourceTypes));

            }
        }

        private bool isenabled_Name;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpName", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Name
        {
            get { return isenabled_Name; }
            set
            {

                isenabled_Name = value;

                RaisePropertyChanged(nameof(IsEnabled_Name));

            }
        }

        private string datatext_Reference;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpReference", ViewItemType = ViewItemType.Content)]
		public string DataText_Reference
        {
            get { return datatext_Reference; }
            set
            {
                if (datatext_Reference == value) return;

                datatext_Reference = value;

                RaisePropertyChanged(nameof(DataText_Reference));

            }
        }

        private string text_Name;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpName", ViewItemType = ViewItemType.Content)]
		public string Text_Name
        {
            get { return text_Name; }
            set
            {
                if (text_Name == value) return;

                text_Name = value;

                RaisePropertyChanged(nameof(Text_Name));

            }
        }

        private bool isenabled_Save;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnSave", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Save
        {
            get { return isenabled_Save; }
            set
            {

                isenabled_Save = value;

                RaisePropertyChanged(nameof(IsEnabled_Save));

            }
        }

        private string datatext_DropDownItemId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.KendoDropDownList, ViewItemId = "dropDownSourceTypes", ViewItemType = ViewItemType.SelectedIndex)]
		public string DataText_DropDownItemId
        {
            get { return datatext_DropDownItemId; }
            set
            {
                if (datatext_DropDownItemId == value) return;

                datatext_DropDownItemId = value;

                RaisePropertyChanged(nameof(DataText_DropDownItemId));

            }
        }

        
    }
}
