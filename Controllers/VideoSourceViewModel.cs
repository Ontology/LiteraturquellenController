﻿using LiteraturquellenController.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Controllers
{
    public class VideoSourceViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(nameof(IsSuccessful_Login));

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(nameof(IsToggled_Listen));

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(nameof(Text_View));

            }
        }

        private string url_InternetSource;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.IFrame, ViewItemId = "iframeInternetSource", ViewItemType = ViewItemType.Content)]
		public string Url_InternetSource
        {
            get { return url_InternetSource; }
            set
            {
                if (url_InternetSource == value) return;

                url_InternetSource = value;

                RaisePropertyChanged(nameof(Url_InternetSource));

            }
        }

        private string url_VideoPlayer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.IFrame, ViewItemId = "iframeVideoPlayer", ViewItemType = ViewItemType.Content)]
		public string Url_VideoPlayer
        {
            get { return url_VideoPlayer; }
            set
            {
                if (url_VideoPlayer == value) return;

                url_VideoPlayer = value;

                RaisePropertyChanged(nameof(Url_VideoPlayer));

            }
        }

        private VideoSource videosource_Item;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "videoSource", ViewItemType = ViewItemType.Other)]
		public VideoSource VideoSource_Item
        {
            get { return videosource_Item; }
            set
            {
                if (videosource_Item == value) return;

                videosource_Item = value;

                RaisePropertyChanged(nameof(VideoSource_Item));

            }
        }
    }
}
