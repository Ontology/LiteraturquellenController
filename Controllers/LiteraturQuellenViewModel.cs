﻿using LiteraturquellenController.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Controllers
{
    public class LiteraturQuellenViewModel : ViewModelBase
    {
        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(nameof(IsToggled_Listen));

            }
        }

        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(nameof(IsSuccessful_Login));

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(nameof(Text_View));

            }
        }

        private Dictionary<string, object> kendoDatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
        public Dictionary<string, object> KendoDataSource_Grid
        {
            get { return kendoDatasource_Grid; }
            set
            {
                if (kendoDatasource_Grid == value) return;

                kendoDatasource_Grid = value;

                RaisePropertyChanged(nameof(KendoDataSource_Grid));

            }
        }

        private SelectedLiteraturQuelle selectedliteraturquelle_Selected;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.SelectedIndex)]
		public SelectedLiteraturQuelle SelectedLiteraturQuelle_Selected
        {
            get { return selectedliteraturquelle_Selected; }
            set
            {
                if (selectedliteraturquelle_Selected == value) return;

                selectedliteraturquelle_Selected = value;

                RaisePropertyChanged(nameof(SelectedLiteraturQuelle_Selected));

            }
        }

        private string url_CreateLiteraturquelle;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Url, ViewItemId = "iframeCreateLiteraturquelle", ViewItemType = ViewItemType.Content)]
		public string Url_CreateLiteraturquelle
        {
            get { return url_CreateLiteraturquelle; }
            set
            {
                if (url_CreateLiteraturquelle == value) return;

                url_CreateLiteraturquelle = value;

                RaisePropertyChanged(nameof(Url_CreateLiteraturquelle));

            }
        }

        private ChangedLiteraturQuelle changedliteraturquelle_LiteraturQuelleAdd;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "addedLiteraturQuelle", ViewItemType = ViewItemType.Other)]
        public ChangedLiteraturQuelle ChangedLiteraturQuelle_LiteraturQuelleAdd
        {
            get { return changedliteraturquelle_LiteraturQuelleAdd; }
            set
            {
                if (changedliteraturquelle_LiteraturQuelleAdd == value) return;

                changedliteraturquelle_LiteraturQuelleAdd = value;

                RaisePropertyChanged(nameof(ChangedLiteraturQuelle_LiteraturQuelleAdd));

            }
        }
    }
}
