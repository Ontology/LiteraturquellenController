﻿using LiteraturquellenController.Services;
using LiteraturquellenController.Translations;
using LoggingServiceController;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using System.ComponentModel;

namespace LiteraturquellenController.Controllers
{
    public class InternetSourceViewController: InternetSourceViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private ElasticServiceAgent elasticServiceAgent;
        private LoggingManager loggingManager;
        private MediaViewerController.Connectors.MediaConnector mediaConnector;

        private TranslationController translationController = new TranslationController();

        private clsOntologyItem referenceItem;

        private clsLocalConfig localConfig;

        private DateTime? dateTimeToSet;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public InternetSourceViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            

            PropertyChanged += InternetSourceViewController_PropertyChanged;
        }

        private void InternetSourceViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            if (e.PropertyName == nameof(IsChecked_Author))
            {
                if (IsChecked_Author)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.SelectedClassNode,
                        OItems = new List<clsOntologyItem> { localConfig.OItem_type_partner }
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
                
            }
            else if (e.PropertyName == nameof(IsChecked_Url))
            {
                if (IsChecked_Url)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.SelectedClassNode,
                        OItems = new List<clsOntologyItem> { localConfig.OItem_type_url }
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

            }

            property.ViewItem.AddValue(property.Property.GetValue(this));

            
           webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;




            elasticServiceAgent = new ElasticServiceAgent(localConfig);
            elasticServiceAgent.PropertyChanged += ElasticServiceAgent_PropertyChanged;

            loggingManager = new LoggingManager(localConfig.Globals);
            loggingManager.PropertyChanged += LoggingManager_PropertyChanged;

            mediaConnector = new MediaViewerController.Connectors.MediaConnector(localConfig.Globals);
            mediaConnector.PropertyChanged += MediaConnector_PropertyChanged;
        }

        private void StateMachine_closedSocket()
        {
            elasticServiceAgent.StopRead();
            webSocketServiceAgent.RemoveAllResources();
            elasticServiceAgent = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AppliedObjects,
                EndpointType = EndpointType.Receiver
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = FileUploadController.Notifications.NotifyChanges.Channel_FilesUploaded,
                EndpointType = EndpointType.Receiver
            });

            if (webSocketServiceAgent.DedicatedSenderArgument != null)
            {
                var viewReadyMessage = new InterServiceMessage
                {
                    ChannelId = Channels.ViewReady,
                    ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value,
                    ViewId = webSocketServiceAgent.IdEntryView
                };

                webSocketServiceAgent.SendInterModMessage(viewReadyMessage);
            }

            
            IsToggled_Listen = true;
            IFrame_PDFViewer = OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl
                    + (OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl.EndsWith("/") ? "" : "/")
                    + localConfig.OItem_object_mediaviewer_pdfviewerjq_html.Name + "?Session="
                    + webSocketServiceAgent.DataText_SessionId
                    + "&Sender="
                    + webSocketServiceAgent.EndpointId;
            webSocketServiceAgent.SetVisibility(true);
            webSocketServiceAgent.SetEnable(false);
            webSocketServiceAgent.SendModel();
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ControllerStateMachine.LoginSuccessful))
            {
                Label_AuthorLabel = translationController.Label_AuthorLabel;
                Label_DownloadLabel = translationController.Label_DownloadLabel;
                Label_UrlLabel = translationController.Label_UrlLabel;

                IsEnabled_AuthorText = false;
                IsEnabled_ChangeAuthor = true;
                IsEnabled_ChangeUrl = true;
                IsEnabled_DownloadPicker = false;
                IsEnabled_UrlText = false;

                IsEnabled_OpenUrl = true;
                
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
                
            }
        }

        private void MediaConnector_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(MediaViewerController.Connectors.MediaConnector.ResultCreateMultimediaItems))
            {
                if (mediaConnector.ResultCreateMultimediaItems.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    
                }
            }
        }

        private void LoggingManager_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LoggingManager.ResultData))
            {
                if (loggingManager.ResultData.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (dateTimeToSet == null)
                    {
                        var LogEntries = loggingManager.LogEntries;

                        if (LogEntries.Any())
                        {
                            DateTime_DownloadPicker = LogEntries.First().DateTimeStamp != null ? LogEntries.First().DateTimeStamp : DateTime.Now;
                        }

                        IsEnabled_DownloadPicker = true;
                    }
                    else
                    {
                        var LogEntries = loggingManager.LogEntries;

                        if (LogEntries.Any())
                        {
                            var logEntry = LogEntries.First();

                            var result = loggingManager.ChangeLogEntryTime(logEntry, dateTimeToSet.Value);
                        }
                        else
                        {
                            var result = loggingManager.CreateLogEntry("download: " + dateTimeToSet.ToString(),
                                localConfig.OItem_token_logstate_download,
                                dateTimeToSet.Value,
                                webSocketServiceAgent.oItemUser,
                                "download",
                                new LogEntryRelation
                                {
                                    Direction = localConfig.Globals.Direction_LeftRight,
                                    RefItem = referenceItem,
                                    RelationType = localConfig.OItem_relationtype_download
                                });
                        }
                    }


                }
            }
            else if (e.PropertyName == nameof(LoggingManager.ResultChangeDateTimeStamp))
            {
                if (loggingManager.ResultChangeDateTimeStamp.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    loggingManager.LogEntries = new List<LoggingServiceController.Models.LogEntry>
                    {
                        loggingManager.ResultChangeDateTimeStamp.LogEntry
                    };
                    DateTime_DownloadPicker = loggingManager.LogEntries.First().DateTimeStamp != null ? loggingManager.LogEntries.First().DateTimeStamp : DateTime.Now;
                }
            }
            else if (e.PropertyName == nameof(LoggingManager.ResultCreateLogEntry))
            {
                if (loggingManager.ResultCreateLogEntry.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    loggingManager.LogEntries = new List<LoggingServiceController.Models.LogEntry>
                    {
                        loggingManager.ResultCreateLogEntry.LogEntry
                    };

                    DateTime_DownloadPicker = loggingManager.LogEntries.First().DateTimeStamp != null ? loggingManager.LogEntries.First().DateTimeStamp : DateTime.Now;
                }
            }
            
            
        }

        private void ElasticServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {


                if (elasticServiceAgent.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }
                if (elasticServiceAgent.ResultData.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var result = loggingManager.GetRelatedLogEntries(new List<clsOntologyItem> { referenceItem }, localConfig.Globals.Direction_LeftRight, localConfig.OItem_relationtype_download);
                
                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (elasticServiceAgent.UrlRels.Any())
                        {
                            DataText_UrlText = elasticServiceAgent.UrlRels.First().Name_Other;
                            IsEnabled_OpenUrl = true;
                        }

                        if (elasticServiceAgent.CreatorRels.Any())
                        {
                            DataText_AuthorText = elasticServiceAgent.CreatorRels.First().Name_Other;
                        }


                        IsEnabled_DownloadPicker = true;
                        IsEnabled_ChangeAuthor = true;
                        IsEnabled_ChangeUrl = true;
                    }
                }
                


            }
            else if (e.PropertyName == nameof(ElasticServiceAgent.ResultSaveAuthor))
            {
                if (elasticServiceAgent.ResultSaveAuthor.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    DataText_AuthorText = elasticServiceAgent.CreatorRels.First().Name_Other;
                }
            }
            else if (e.PropertyName == nameof(ElasticServiceAgent.ResultSaveUrl))
            {
                if (elasticServiceAgent.ResultSaveUrl.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    DataText_UrlText = elasticServiceAgent.UrlRels.First().Name_Other;
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            this.webSocketServiceAgent.receivedData += WebSocketServiceAgent_receivedData;


        }

        private void WebSocketServiceAgent_receivedData(byte[] data)
        {
            
           
        }

       

       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                

                Initialize();
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "clicked.btnChangeUrl")
                {
                    IsChecked_Url = !IsChecked_Url;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clicked.btnChangeAuthor")
                {
                    IsChecked_Author = !IsChecked_Author;
                }

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemClass == "DateTime" && changedItem.ViewItemType == "Content")
                    {
                        var date = (DateTime)changedItem.ViewItemValue;
                        date = date.ToLocalTime();

                        dateTimeToSet = date;
                        var result = loggingManager.GetRelatedLogEntries(new List<clsOntologyItem> { referenceItem }, localConfig.Globals.Direction_LeftRight, localConfig.OItem_relationtype_download);
                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            dateTimeToSet = null;
                            if (loggingManager.LogEntries != null && loggingManager.LogEntries.Any())
                            {
                                DateTime_DownloadPicker = loggingManager.LogEntries.First().DateTimeStamp != null ? loggingManager.LogEntries.First().DateTimeStamp : DateTime.Now;
                            }
                            else
                            {
                                DateTime_DownloadPicker = null;
                            }
                        }
                    }

                });

            }

        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (!IsSuccessful_Login) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;
                if (objectItem.GUID_Parent != localConfig.OItem_type_internet_quellenangabe.GUID) return;

                referenceItem = objectItem;

                Url_UploadUrl = ModuleDataExchanger.GetViewUrlById("1389c3c175984954a1a38dcc83f7472c", webSocketServiceAgent.EndpointId) + "&mimeAccept=" + MimeTypes.application_pdf;

                IsEnabled_AuthorText = false;
                IsEnabled_DownloadPicker = false;
                IsEnabled_UrlText = false;

                DataText_AuthorText = "";
                DataText_UrlText = "";

                webSocketServiceAgent.SendInterModMessage(new InterServiceMessage
                {
                    ChannelId = Notifications.NotifyChanges.Channel_PDFViewer,
                    OItems = new List<clsOntologyItem>
                    {
                        referenceItem
                    }
                });

                elasticServiceAgent.GetSourceDataByInternetSource(referenceItem);
            }
            else if (message.ChannelId == Channels.AppliedObjects && (IsChecked_Author || IsChecked_Url) && referenceItem != null)
            {
                var objectItem = message.OItems.FirstOrDefault();

                if (objectItem == null) return;

                if (objectItem.GUID_Parent == localConfig.OItem_type_partner.GUID && IsChecked_Author)
                {
                    var resultTask = elasticServiceAgent.SaveAuthorOfInternetSource(objectItem, referenceItem);
                }
                else if (objectItem.GUID_Parent == localConfig.OItem_type_url.GUID && IsChecked_Url)
                {
                    var resultTask = elasticServiceAgent.SaveUrlOfInternetSource(objectItem, referenceItem);
                }
            }
            else if (message.ChannelId == FileUploadController.Notifications.NotifyChanges.Channel_FilesUploaded && referenceItem != null)
            {
                var fileItems = message.OItems;

                if (fileItems == null || !fileItems.Any()) return;


                if (!fileItems.All(fileItem => fileItem.Additional1.ToLower() == MimeTypes.application_pdf.ToLower()))
                {
                    return;
                }

                var resultTask = mediaConnector.CreateMultimediaItems(MediaViewerController.Connectors.MediaType.PDF,
                    fileItems,
                    referenceItem);
            }

        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
