﻿using LiteraturquellenController.Models;
using LiteraturquellenController.Services;
using LiteraturquellenController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LiteraturquellenController.Controllers
{
    public class NewLiteraturquelleViewController : NewLiteraturquelleViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ElasticServiceAgent serviceAgentElastic;

        private clsLocalConfig localConfig;

        private clsOntologyItem oItemRef;
        private LiteraturQuelle literaturQuelle;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public NewLiteraturquelleViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();

            PropertyChanged += NewLiteraturquelleViewController_PropertyChanged;
        }

        private void NewLiteraturquelleViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            if (e.PropertyName == nameof(Text_Name) ||
                e.PropertyName == nameof(DataText_Reference) ||
                e.PropertyName == nameof(DataText_DropDownItemId))
            {
                CheckData();
            }


            if (e.PropertyName == nameof(Text_Name) && IsEnabled_Save) return;
            
            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ElasticServiceAgent(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

         
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            IsEnabled_Name = true;
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndpointType = EndpointType.Receiver,
            });

            IsToggled_Listen = true;

            Text_View = translationController.Text_ViewNewLiteraturquelle;

            KendoDropDownConfig_SourceTypes = new OntoWebCore.Models.KendoDropDownConfig
            {
                optionLabel = "Quelltyp",
                dataSource = new List<OntoWebCore.Models.KendoDropDownItem>
                {
                    new OntoWebCore.Models.KendoDropDownItem
                    {
                        Value = localConfig.OItem_type_audio_quelle.GUID,
                        Text = localConfig.OItem_type_audio_quelle.Name
                    },
                    new OntoWebCore.Models.KendoDropDownItem
                    {
                        Value = localConfig.OItem_type_bild_quelle.GUID,
                        Text = localConfig.OItem_type_bild_quelle.Name
                    },
                    new OntoWebCore.Models.KendoDropDownItem
                    {
                        Value = localConfig.OItem_type_buch_quellenangabe.GUID,
                        Text = localConfig.OItem_type_buch_quellenangabe.Name
                    },
                    new OntoWebCore.Models.KendoDropDownItem
                    {
                        Value = localConfig.OItem_type_email_quelle.GUID,
                        Text = localConfig.OItem_type_email_quelle.Name
                    },
                    new OntoWebCore.Models.KendoDropDownItem
                    {
                        Value = localConfig.OItem_type_internet_quellenangabe.GUID,
                        Text = localConfig.OItem_type_internet_quellenangabe.Name
                    },
                    new OntoWebCore.Models.KendoDropDownItem
                    {
                        Value = localConfig.OItem_type_video_quelle.GUID,
                        Text = localConfig.OItem_type_video_quelle.Name
                    },
                    new OntoWebCore.Models.KendoDropDownItem
                    {
                        Value = localConfig.OItem_type_zeitungsquelle.GUID,
                        Text = localConfig.OItem_type_zeitungsquelle.Name
                    }
                }
            };
            
            CheckSendOrParamObject(oItemRef?.GUID);
            CheckData();
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {

        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ControllerStateMachine.LoginSuccessful))
            {
                IsEnabled_Name = false;
                IsEnabled_Save = false;
                
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
                
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ElasticServiceAgent.ResultCreateQuelle))
            {
                if (serviceAgentElastic.ResultCreateQuelle.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    literaturQuelle = serviceAgentElastic.ResultCreateQuelle.LiteraturQuelle;

                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Notifications.NotifyChanges.Channel_CreatedLiteraturQuelle,
                        GenericParameterItems = new List<object>
                        {
                            literaturQuelle
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

        private void CheckData()
        {
            IsEnabled_Save = false;


            if (literaturQuelle != null) return;
            if (string.IsNullOrEmpty(Text_Name)) return;
            if (string.IsNullOrEmpty(DataText_DropDownItemId)) return;
            if (!localConfig.Globals.is_GUID(DataText_DropDownItemId)) return;

            if (!(DataText_DropDownItemId == localConfig.OItem_type_audio_quelle.GUID ||
                  DataText_DropDownItemId == localConfig.OItem_type_bild_quelle.GUID ||
                  DataText_DropDownItemId == localConfig.OItem_type_buch_quellenangabe.GUID ||
                  DataText_DropDownItemId == localConfig.OItem_type_email_quelle.GUID ||
                  DataText_DropDownItemId == localConfig.OItem_type_internet_quellenangabe.GUID ||
                  DataText_DropDownItemId == localConfig.OItem_type_literarische_quelle.GUID ||
                  DataText_DropDownItemId == localConfig.OItem_type_video_quelle.GUID ||
                  DataText_DropDownItemId == localConfig.OItem_type_zeitungsquelle.GUID)) return;

            IsEnabled_Save = true;

        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "clicked.listen")
                {
                    IsToggled_Listen = !IsToggled_Listen;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clicked.save")
                {
                    clsOntologyItem oItemQuellType = null;
                    if (DataText_DropDownItemId == localConfig.OItem_type_audio_quelle.GUID)
                    {
                        oItemQuellType = localConfig.OItem_type_audio_quelle;
                    }
                    else if (DataText_DropDownItemId == localConfig.OItem_type_bild_quelle.GUID)
                    {
                        oItemQuellType = localConfig.OItem_type_bild_quelle;
                    }
                    else if (DataText_DropDownItemId == localConfig.OItem_type_buch_quellenangabe.GUID)
                    {
                        oItemQuellType = localConfig.OItem_type_buch_quellenangabe;
                    }
                    else if (DataText_DropDownItemId == localConfig.OItem_type_email_quelle.GUID)
                    {
                        oItemQuellType = localConfig.OItem_type_email_quelle;
                    }
                    else if (DataText_DropDownItemId == localConfig.OItem_type_internet_quellenangabe.GUID)
                    {
                        oItemQuellType = localConfig.OItem_type_internet_quellenangabe;
                    }
                    else if (DataText_DropDownItemId == localConfig.OItem_type_literarische_quelle.GUID)
                    {
                        oItemQuellType = localConfig.OItem_type_literarische_quelle;
                    }
                    else if (DataText_DropDownItemId == localConfig.OItem_type_video_quelle.GUID)
                    {
                        oItemQuellType = localConfig.OItem_type_video_quelle;
                    }
                    else if (DataText_DropDownItemId == localConfig.OItem_type_zeitungsquelle.GUID)
                    {
                        oItemQuellType = localConfig.OItem_type_zeitungsquelle;
                    }

                    if (oItemQuellType == null) return;

                    var resultTask = serviceAgentElastic.CreateLiteraturQuelle(oItemQuellType, Text_Name, oItemRef);
                }
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    

                });
                

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var objectId = webSocketServiceAgent.ObjectArgument.Value.ToString();
                if (!localConfig.Globals.is_GUID(objectId)) return;
                oItemRef = serviceAgentElastic.GetOItem(objectId, localConfig.Globals.Type_Object);
                if (!IsSuccessful_Login) return;
                if (!IsToggled_Listen) return;
                
                CheckSendOrParamObject(objectId);
            }



        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (!IsSuccessful_Login) return;
            var oItemMessage = message.OItems.LastOrDefault();
            if (oItemMessage == null) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var oitemMessage = message.OItems.FirstOrDefault();

                if (oItemMessage == null) return;

                oItemRef = oItemMessage;

                IsToggled_Listen = false;

                CheckSendOrParamObject(oItemRef.GUID);

                
            }


        }

        private void CheckSendOrParamObject(string idItem)
        {

            if (string.IsNullOrEmpty(idItem)) return;
            var paramObject = serviceAgentElastic.GetOItem(idItem, localConfig.Globals.Type_Object);

            if (paramObject == null) return;

            oItemRef = paramObject;

            Text_View = oItemRef.Name;
            if (oItemRef.Type == localConfig.Globals.Type_Object)
            {
                var classItem = serviceAgentElastic.GetOItem(oItemRef.GUID_Parent, localConfig.Globals.Type_Class);
                if (classItem == null) return;
                Text_View += " (" + classItem.Name + ")";
            }

            DataText_Reference = oItemRef.Name;
            
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }

    }
}
