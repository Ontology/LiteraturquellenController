﻿using LiteraturquellenController.Factories;
using LiteraturquellenController.Models;
using LiteraturquellenController.Services;
using LiteraturquellenController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LiteraturquellenController.Controllers
{
    public class LiteraturQuellenController : LiteraturQuellenViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ElasticServiceAgent serviceAgentElastic;

        private LiteraturQuellenFactory literaturQuellenFactory;


        private clsLocalConfig localConfig;

        private clsOntologyItem oItemRef;

        private bool firstLoad = false;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public LiteraturQuellenController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += LiteraturQuellenController_PropertyChanged;
        }

        private void LiteraturQuellenController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            if (e.PropertyName == nameof(SelectedLiteraturQuelle_Selected))
            {
                clsOntologyItem sendItem = null;
                if (SelectedLiteraturQuelle_Selected.Property == nameof(LiteraturQuelle.NameQuelle))
                {
                    sendItem = new clsOntologyItem
                    {
                        GUID = SelectedLiteraturQuelle_Selected.LiteraturQuelle.IdQuelle,
                        Name = SelectedLiteraturQuelle_Selected.LiteraturQuelle.NameQuelle,
                        GUID_Parent = SelectedLiteraturQuelle_Selected.LiteraturQuelle.IdClassQuelle,
                        Type = localConfig.Globals.Type_Object
                    };

                }
                else if (SelectedLiteraturQuelle_Selected.Property == nameof(LiteraturQuelle.NameLiteraturQuelle))
                {
                    sendItem = new clsOntologyItem
                    {
                        GUID = SelectedLiteraturQuelle_Selected.LiteraturQuelle.IdLiteraturQuelle,
                        Name = SelectedLiteraturQuelle_Selected.LiteraturQuelle.NameLiteraturQuelle,
                        GUID_Parent = localConfig.OItem_type_literarische_quelle.GUID,
                        Type = localConfig.Globals.Type_Object
                    };
                }

                if (sendItem == null) return;

                var interModMessage = new InterServiceMessage
                {
                    ChannelId = Channels.ParameterList,
                    OItems = new List<clsOntologyItem> { sendItem }
                };

                webSocketServiceAgent.SendInterModMessage(interModMessage);
            }

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ElasticServiceAgent(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            literaturQuellenFactory = new LiteraturQuellenFactory(localConfig);
            literaturQuellenFactory.PropertyChanged += LiteraturQuellenFactory_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndpointType = EndpointType.Receiver,
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Notifications.NotifyChanges.Channel_CreatedLiteraturQuelle,
                EndpointType = EndpointType.Receiver,
            });

            IsToggled_Listen = true;

            Text_View = translationController.Text_ViewLiteraturQuellen;

            
            FirstLoad();
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void LiteraturQuellenFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LiteraturQuellenFactory.ResultLiteraturQuellen))
            {
                if (literaturQuellenFactory.ResultLiteraturQuellen.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                    var resultTask = GridFactory.WriteKendoJson(typeof(LiteraturQuelle), literaturQuellenFactory.ResultLiteraturQuellen.LiteraturQuellen.ToList<object>(), sessionFile);

                    resultTask.Wait();

                    if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        KendoDataSource_Grid = GridFactory.CreateKendoGridConfig(typeof(LiteraturQuelle), sessionFile);
                    }
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ElasticServiceAgent.ResultLiteraturSource))
            {
                if (serviceAgentElastic.ResultLiteraturSource.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var resultTask = literaturQuellenFactory.CreateLiteraturQuellenList(serviceAgentElastic.ResultLiteraturSource);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            this.webSocketServiceAgent.paramsChecked += WebSocketServiceAgent_paramsChecked;

        }

        private void WebSocketServiceAgent_paramsChecked()
        {
            FirstLoad();



        }

        private void FirstLoad()
        {
            if (!IsSuccessful_Login) return;
            if (firstLoad) return;
            firstLoad = true;
            var resultTask = serviceAgentElastic.GetLiteraturSources(oItemRef);
        }

        
       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "clicked.listen")
                {
                    IsToggled_Listen = !IsToggled_Listen;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clicked.addSource")
                {
                    var viewUrl = ModuleDataExchanger.GetViewUrlById("61f3da21b6a04e7a8e4c91d36fccf395", sender: webSocketServiceAgent.EndpointId, objectId: oItemRef?.GUID);
                    Url_CreateLiteraturquelle = viewUrl;
                }
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemClass == "Grid" && changedItem.ViewItemType == "SelectedIndex")
                    {
                        var selected = Newtonsoft.Json.JsonConvert.DeserializeObject<SelectedLiteraturQuelle>(changedItem.ViewItemValue.ToString());

                        if (selected == null) return;

                        SelectedLiteraturQuelle_Selected = selected;

                    }

                });

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var objectId = webSocketServiceAgent.ObjectArgument.Value.ToString();
                if (!localConfig.Globals.is_GUID(objectId)) return;
                if (!IsSuccessful_Login) return;
                if (!IsToggled_Listen) return;


            }



        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (!IsSuccessful_Login) return;
            
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var oitemMessage = message.OItems.FirstOrDefault();

                if (oitemMessage == null) return;

                oItemRef = oitemMessage;

                IsToggled_Listen = false;

                CheckSendOrParamObject(oItemRef.GUID);

                var resultTask = serviceAgentElastic.GetLiteraturSources(oItemRef);
            }
            else if (message.ChannelId == Notifications.NotifyChanges.Channel_CreatedLiteraturQuelle)
            {
                var literaturQuelleObject = message.GenericParameterItems.FirstOrDefault();

                if (literaturQuelleObject == null) return;


                var literaturQuelle = Newtonsoft.Json.JsonConvert.DeserializeObject<LiteraturQuelle>(literaturQuelleObject.ToString());

                ChangedLiteraturQuelle_LiteraturQuelleAdd = new ChangedLiteraturQuelle
                {
                    LiteraturQuelle = literaturQuelle
                };


            }


        }

        private void CheckSendOrParamObject(string idItem)
        {

            var paramObject = serviceAgentElastic.GetOItem(idItem, localConfig.Globals.Type_Object);

            if (paramObject == null) return;

            oItemRef = paramObject;

            Text_View = oItemRef.Name;
            if (oItemRef.Type == localConfig.Globals.Type_Object)
            {
                var classItem = serviceAgentElastic.GetOItem(oItemRef.GUID_Parent, localConfig.Globals.Type_Class);
                if (classItem == null) return;
                Text_View += " (" + classItem.Name + ")";
            }

        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }

    }
}
