﻿using LiteraturquellenController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturquellenController.Controllers
{
    public class InternetSourceViewModel : OntoMsg_Module.Base.ViewModelBase
    {

        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true)]
        public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string label_UrlLabel;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "lblUrl", ViewItemClass = ViewItemClass.Label)]
        public string Label_UrlLabel
        {
            get { return label_UrlLabel; }
            set
            {
                if (label_UrlLabel == value) return;

                label_UrlLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_UrlLabel);

            }
        }

        private bool isvisible_UrlLabel;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Visible, ViewItemId = "lblUrl", ViewItemClass = ViewItemClass.Label)]
        public bool IsVisible_UrlLabel
        {
            get { return isvisible_UrlLabel; }
            set
            {
                if (isvisible_UrlLabel == value) return;

                isvisible_UrlLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_UrlLabel);

            }
        }

        private string datatext_UrlText;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "inpUrl", ViewItemClass = ViewItemClass.Input)]
        public string DataText_UrlText
        {
            get { return datatext_UrlText; }
            set
            {
                if (datatext_UrlText == value) return;

                datatext_UrlText = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_UrlText);

            }
        }

        private bool isenabled_UrlText;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Enable, ViewItemId = "inpUrl", ViewItemClass = ViewItemClass.Input)]
        public bool IsEnabled_UrlText
        {
            get { return isenabled_UrlText; }
            set
            {

                isenabled_UrlText = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_UrlText);

            }
        }

        private bool isvisible_UrlText;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Visible, ViewItemId = "inpUrl", ViewItemClass = ViewItemClass.Input)]
        public bool IsVisible_UrlText
        {
            get { return isvisible_UrlText; }
            set
            {
                if (isvisible_UrlText == value) return;

                isvisible_UrlText = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_UrlText);

            }
        }

        private string label_ChangeUrl;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "btnChangeUrl", ViewItemClass = ViewItemClass.ToggleButton)]
        public string Label_ChangeUrl
        {
            get { return label_ChangeUrl; }
            set
            {
                if (label_ChangeUrl == value) return;

                label_ChangeUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ChangeUrl);

            }
        }

        private bool isenabled_ChangeUrl;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Enable, ViewItemId = "btnChangeUrl", ViewItemClass = ViewItemClass.ToggleButton)]
        public bool IsEnabled_ChangeUrl
        {
            get { return isenabled_ChangeUrl; }
            set
            {

                isenabled_ChangeUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ChangeUrl);

            }
        }

        private bool isvisible_ChangeUrl;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Visible, ViewItemId = "btnChangeUrl", ViewItemClass = ViewItemClass.ToggleButton)]
        public bool IsVisible_ChangeUrl
        {
            get { return isvisible_ChangeUrl; }
            set
            {
                if (isvisible_ChangeUrl == value) return;

                isvisible_ChangeUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ChangeUrl);

            }
        }

        private string label_DownloadLabel;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "lblDownload", ViewItemClass = ViewItemClass.Label)]
        public string Label_DownloadLabel
        {
            get { return label_DownloadLabel; }
            set
            {
                if (label_DownloadLabel == value) return;

                label_DownloadLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_DownloadLabel);

            }
        }

        private bool isvisible_DownloadLabel;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Visible, ViewItemId = "lblDownload", ViewItemClass = ViewItemClass.Label)]
        public bool IsVisible_DownloadLabel
        {
            get { return isvisible_DownloadLabel; }
            set
            {
                if (isvisible_DownloadLabel == value) return;

                isvisible_DownloadLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_DownloadLabel);

            }
        }

        private DateTime? datetime_DownloadPicker;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "dateDownload", ViewItemClass = ViewItemClass.DateTime)]
        public DateTime? DateTime_DownloadPicker
        {
            get { return datetime_DownloadPicker; }
            set
            {
                if (datetime_DownloadPicker == value) return;

                datetime_DownloadPicker = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DateTime_DownloadPicker);

            }
        }

        private bool isenabled_DownloadPicker;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Enable, ViewItemId = "dateDownload", ViewItemClass = ViewItemClass.DateTime)]
        public bool IsEnabled_DownloadPicker
        {
            get { return isenabled_DownloadPicker; }
            set
            {

                isenabled_DownloadPicker = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_DownloadPicker);

            }
        }

        private bool isvisible_DownloadPicker;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Visible, ViewItemId = "dateDownload", ViewItemClass = ViewItemClass.DateTime)]
        public bool IsVisible_DownloadPicker
        {
            get { return isvisible_DownloadPicker; }
            set
            {
                if (isvisible_DownloadPicker == value) return;

                isvisible_DownloadPicker = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_DownloadPicker);

            }
        }

        private string label_AuthorLabel;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "lblAuthor", ViewItemClass = ViewItemClass.Label)]
        public string Label_AuthorLabel
        {
            get { return label_AuthorLabel; }
            set
            {
                if (label_AuthorLabel == value) return;

                label_AuthorLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_AuthorLabel);

            }
        }

        private bool isvisible_AuthorLabel;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Visible, ViewItemId = "lblAuthor", ViewItemClass = ViewItemClass.Label)]
        public bool IsVisible_AuthorLabel
        {
            get { return isvisible_AuthorLabel; }
            set
            {
                if (isvisible_AuthorLabel == value) return;

                isvisible_AuthorLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_AuthorLabel);

            }
        }

        private string datatext_AuthorText;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "inpAuthor", ViewItemClass = ViewItemClass.Input)]
        public string DataText_AuthorText
        {
            get { return datatext_AuthorText; }
            set
            {
                if (datatext_AuthorText == value) return;

                datatext_AuthorText = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AuthorText);

            }
        }

        private bool isenabled_AuthorText;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Enable, ViewItemId = "inpAuthor", ViewItemClass = ViewItemClass.Input)]
        public bool IsEnabled_AuthorText
        {
            get { return isenabled_AuthorText; }
            set
            {

                isenabled_AuthorText = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_AuthorText);

            }
        }

        private bool isvisible_AuthorText;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Visible, ViewItemId = "inpAuthor", ViewItemClass = ViewItemClass.Input)]
        public bool IsVisible_AuthorText
        {
            get { return isvisible_AuthorText; }
            set
            {
                if (isvisible_AuthorText == value) return;

                isvisible_AuthorText = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_AuthorText);

            }
        }

        private string label_ChangeAuthor;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "btnChangeAuthor", ViewItemClass = ViewItemClass.ToggleButton)]
        public string Label_ChangeAuthor
        {
            get { return label_ChangeAuthor; }
            set
            {
                if (label_ChangeAuthor == value) return;

                label_ChangeAuthor = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ChangeAuthor);

            }
        }

        private bool isenabled_ChangeAuthor;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Enable, ViewItemId = "btnChangeAuthor", ViewItemClass = ViewItemClass.ToggleButton)]
        public bool IsEnabled_ChangeAuthor
        {
            get { return isenabled_ChangeAuthor; }
            set
            {

                isenabled_ChangeAuthor = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ChangeAuthor);

            }
        }

        private bool isvisible_ChangeAuthor;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Visible, ViewItemId = "btnChangeAuthor", ViewItemClass = ViewItemClass.ToggleButton)]
        public bool IsVisible_ChangeAuthor
        {
            get { return isvisible_ChangeAuthor; }
            set
            {
                if (isvisible_ChangeAuthor == value) return;

                isvisible_ChangeAuthor = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ChangeAuthor);

            }
        }

        private string label_OpenUrl;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "btnOpenUrl", ViewItemClass = ViewItemClass.ImageButton)]
        public string Label_OpenUrl
        {
            get { return label_OpenUrl; }
            set
            {
                if (label_OpenUrl == value) return;

                label_OpenUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_OpenUrl);

            }
        }

        private bool isvisible_OpenUrl;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Visible, ViewItemId = "btnOpenUrl", ViewItemClass = ViewItemClass.ImageButton)]
        public bool IsVisible_OpenUrl
        {
            get { return isvisible_OpenUrl; }
            set
            {
                if (isvisible_OpenUrl == value) return;

                isvisible_OpenUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_OpenUrl);

            }
        }

        private bool isenabled_OpenUrl;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Enable, ViewItemId = "btnOpenUrl", ViewItemClass = ViewItemClass.ImageButton)]
        public bool IsEnabled_OpenUrl
        {
            get { return isenabled_OpenUrl; }
            set
            {
                if (isenabled_OpenUrl == value) return;

                isenabled_OpenUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_OpenUrl);

            }
        }

        private string iframe_PDFViewer;
        [ViewModel(Send = true, ViewItemType = ViewItemType.Content, ViewItemId = "iframePDFViewer", ViewItemClass = ViewItemClass.IFrame)]
        public string IFrame_PDFViewer
        {
            get { return iframe_PDFViewer; }
            set
            {
                if (iframe_PDFViewer == value) return;

                iframe_PDFViewer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IFrame_PDFViewer);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }

        private bool ischecked_Author;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "btnChangeAuthor", ViewItemType = ViewItemType.Checked)]
		public bool IsChecked_Author
        {
            get { return ischecked_Author; }
            set
            {
                if (ischecked_Author == value) return;

                ischecked_Author = value;

                RaisePropertyChanged(nameof(IsChecked_Author));

            }
        }

        private bool ischecked_Url;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "btnChangeUrl", ViewItemType = ViewItemType.Checked)]
		public bool IsChecked_Url
        {
            get { return ischecked_Url; }
            set
            {
                if (ischecked_Url == value) return;

                ischecked_Url = value;

                RaisePropertyChanged(nameof(IsChecked_Url));

            }
        }

        private string url_UploadUrl;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Url, ViewItemId = "iframeFileUpload", ViewItemType = ViewItemType.Other)]
		public string Url_UploadUrl
        {
            get { return url_UploadUrl; }
            set
            {
                if (url_UploadUrl == value) return;

                url_UploadUrl = value;

                RaisePropertyChanged(nameof(Url_UploadUrl));

            }
        }
    }
}
