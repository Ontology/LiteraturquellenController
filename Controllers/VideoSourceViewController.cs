﻿using LiteraturquellenController.Factories;
using LiteraturquellenController.Services;
using LiteraturquellenController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LiteraturquellenController.Controllers
{
    public class VideoSourceViewController : VideoSourceViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ElasticServiceAgent serviceAgentElastic;
        private VideoSourceFactory videoSourceFactory;

        private clsLocalConfig localConfig;

        private clsOntologyItem videoSource;

        private string paramObjectId;
        private bool videoPlayerReady;
        private string videoPlayerEndpointId;
        private bool internetSourceReady;
        private string internetSourceEndpointId;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public VideoSourceViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();

            PropertyChanged += VideoSourceViewController_PropertyChanged;
        }

        private void VideoSourceViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

        
            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ElasticServiceAgent(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            videoSourceFactory = new VideoSourceFactory();
            videoSourceFactory.PropertyChanged += VideoSourceFactory_PropertyChanged;
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndpointType = EndpointType.Receiver,
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ViewReady,
                EndpointType = EndpointType.Receiver,
            });

            IsToggled_Listen = true;

            Text_View = translationController.Text_ViewNewLiteraturquelle;

            if (videoSource == null && !string.IsNullOrEmpty(paramObjectId))
            {
                CheckSendOrParamObject(paramObjectId);
            }

            var videoViewerUrl = ModuleDataExchanger.GetViewUrlById("0ecd03c9162c45839331260af4336af7", sender: webSocketServiceAgent.EndpointId);
            var internetSourceUrl = ModuleDataExchanger.GetViewUrlById("ecbd7a4060974a01be7dbcacc18dc8c8", sender: webSocketServiceAgent.EndpointId);

            Url_VideoPlayer = videoViewerUrl;
            Url_InternetSource = internetSourceUrl;
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void VideoSourceFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(VideoSourceFactory.ResultVideoSource))
            {
                if (videoSourceFactory.ResultVideoSource.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    VideoSource_Item = videoSourceFactory.ResultVideoSource.VideoSource;
                    SendToSubViews();
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ElasticServiceAgent.VideoSourceResult))
            {
                if (serviceAgentElastic.VideoSourceResult.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    

                    var resultTask = videoSourceFactory.CreateVideoSourceListAndWriteToJson(serviceAgentElastic.VideoSourceResult);

                }
            }
        }

        private void SendToSubViews()
        {
            if (internetSourceReady && videoPlayerReady && VideoSource_Item != null)
            {
                if (VideoSource_Item.Video != null && !string.IsNullOrEmpty(VideoSource_Item.Video.IdVideo))
                {
                    var oItemVideo = serviceAgentElastic.GetOItem(VideoSource_Item.Video.IdVideo, localConfig.Globals.Type_Object);

                    if (oItemVideo == null) return;

                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        ReceiverId = videoPlayerEndpointId,
                        OItems = new List<clsOntologyItem>
                        {
                            oItemVideo
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

                if (!string.IsNullOrEmpty(VideoSource_Item.IdInternetSource))
                {
                    var oItemInternetSource = serviceAgentElastic.GetOItem(VideoSource_Item.IdInternetSource, localConfig.Globals.Type_Object);

                    if (oItemInternetSource == null) return;

                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        ReceiverId = internetSourceEndpointId,
                        OItems = new List<clsOntologyItem>
                        {
                            oItemInternetSource
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }


        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {


                });


            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                paramObjectId = webSocketServiceAgent.ObjectArgument.Value.ToString();
                if (!localConfig.Globals.is_GUID(paramObjectId)) return;

                if (!IsSuccessful_Login) return;

                CheckSendOrParamObject(paramObjectId);
            }

            

        }

        private void CheckSendOrParamObject(string idItem)
        {

            if (string.IsNullOrEmpty(idItem)) return;
            var paramObject = serviceAgentElastic.GetOItem(idItem, localConfig.Globals.Type_Object);

            if (paramObject == null) return;

            if (paramObject.GUID_Parent != localConfig.OItem_type_video_quelle.GUID) return;

            videoSource = paramObject;

            Text_View = videoSource.Name;
            Text_View += " (" + localConfig.OItem_type_literarische_quelle.Name + ")";

            var resultTask = serviceAgentElastic.GetSourceDataByVideoSource(videoSource);

        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (!IsSuccessful_Login) return;
            
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;

                var oitemMessage = message.OItems.FirstOrDefault();

                if (oItemMessage == null) return;

                CheckSendOrParamObject(oItemMessage.GUID);


            }
            else if (message.ChannelId == Channels.ViewReady && message.ReceiverId == webSocketServiceAgent.EndpointId)
            {
                if (message.ViewId == "0ecd03c9162c45839331260af4336af7")
                {
                    videoPlayerReady = true;
                    videoPlayerEndpointId = message.SenderId;
                    SendToSubViews();
                }
                else if (message.ViewId == "ecbd7a4060974a01be7dbcacc18dc8c8")
                {
                    internetSourceReady = true;
                    internetSourceEndpointId = message.SenderId;
                    SendToSubViews();
                }
            }


        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
